﻿
-- Indicar el nombre del club que tiene pescadores

  SELECT DISTINCT c.nombre FROM clubes c
  JOIN pescadores p ON c.cif = p.club_cif;

-- Indicar el nombre del club que no tiene pescadores

  SELECT c.nombre FROM clubes c
  LEFT JOIN pescadores p ON c.cif = p.club_cif
  WHERE p.club_cif IS NULL;

-- Indicar los clubes no autorizados

  SELECT c.nombre FROM  clubes c
  LEFT JOIN autorizados a ON c.cif = a.club_cif
  WHERE a.club_cif IS NULL;
  
-- Indicar el nombre de los cotos autorizados a algún club.
  
  SELECT a.coto_nombre FROM autorizados a;

-- Indicar la provincia que tiene cotos autorizados a algún club.

  SELECT DISTINCT c.provincia FROM cotos c
  JOIN autorizados a ON c.nombre = a.coto_nombre;

-- Indicar el nombre de los cotos que no están autorizados a ningún club.

  SELECT c.nombre FROM cotos c
  LEFT JOIN autorizados a ON c.nombre = a.coto_nombre
  WHERE a.coto_nombre IS NULL;

-- Indicar el nº de rios por provincia con cotos.

  SELECT c.provincia,COUNT(DISTINCT c.rio) FROM cotos c
  GROUP BY c.provincia;

-- Indicar el nº de rios por provincia con cotos autorizados.

  SELECT c.provincia,COUNT(DISTINCT c.rio)nºrios FROM cotos c
  JOIN autorizados a ON c.nombre= a.coto_nombre
  GROUP BY c.provincia;
  
-- Indicar el nombre de la provincia con más cotos autorizados.
  
  -- c1: nº de cotos autorizados por provincia
  SELECT c.provincia, c.nombre, COUNT(*)nº FROM  cotos c
  JOIN autorizados a ON c.nombre= a.coto_nombre
  GROUP BY c.provincia;
  -- c2: 
    SELECT MAX(c1.numero) maximo FROM (
SELECT c.provincia, c.nombre, COUNT(*)nº FROM  cotos c
  JOIN autorizados a ON c.nombre= a.coto_nombre
  GROUP BY c.provincia) c1;

 -- final

  SELECT * FROM 
    (SELECT c.provincia, c.nombre, COUNT(*)nº FROM  cotos c
  JOIN autorizados a ON c.nombre= a.coto_nombre
  GROUP BY c.provincia
    )c1
    JOIN
    (SELECT MAX(c1.nº) maximo FROM (
SELECT c.provincia, c.nombre, COUNT(*)nº FROM  cotos c
  JOIN autorizados a ON c.nombre= a.coto_nombre
  GROUP BY c.provincia) c1
    ) c2
    ON c1.nº=c2.maximo;
